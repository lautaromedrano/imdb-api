# IMDB TOP 1000 SEARCHER

## Description

IMDB TOP 1000 movies searcher.

Interviewer: The Yes

## Installation

```bash
$ npm install
```

## Running the app

```bash
$ npm run start
```

Get request to http://localhost:3000/?search=KEYWORD(s)

## Tech Stack
- NestJs
- Axios
- Cheerio
- In memory db composed by a reversed index and a hash table