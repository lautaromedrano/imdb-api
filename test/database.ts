import { DatabaseService } from '../src/db/database.service';

test('DB basic consistency', () => {
    const databaseService = new DatabaseService();

    databaseService.storeKeyword('a1', 'first word');
    databaseService.storeKeyword('a2', 'second word');

    expect(databaseService.fetchUnion(['a1'])).toStrictEqual(['first word']);
    expect(databaseService.fetchUnion(['a2'])).toStrictEqual(['second word']);
});

test('DB does not store each item more than once', () => {
    const databaseService = new DatabaseService();

    databaseService.storeKeyword('a1', 'first word');
    databaseService.storeKeyword('a1', 'first word');

    expect(databaseService.fetchUnion(['a1'])).toStrictEqual(['first word']);
});

test('DB return empty array if there is no match', () => {
    const databaseService = new DatabaseService();

    databaseService.storeKeyword('a1', 'first word');

    expect(databaseService.fetchUnion(['a2'])).toStrictEqual([]);
});
