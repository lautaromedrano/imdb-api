import { ParserService } from '../src/module/parser/parser.service';

const parserService = new ParserService();

test('parse splits tokens by all defined separators', () => {
    expect(parserService.parse('a1 b1.c1,d1:e1;f1?g1!h1<i1>j1"k1'))
        .toStrictEqual(['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1', 'i1', 'j1', 'k1']);
});

test('parse splits skips stop words', () => {
    expect(parserService.parse('The lord of the rings'))
        .toStrictEqual(['lord', 'rings']);
});

test('parse all stop words returns empty array', () => {
    expect(parserService.parse('The a of the on'))
        .toStrictEqual([]);
});
