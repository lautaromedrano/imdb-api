export class MovieDTO {
  constructor(
    private readonly title: string,
    private readonly director: string,
    private readonly summary: string,
    private readonly actors: string[],
  ) {}

  getTitle(): string {
    return this.title;
  }

  getDirector(): string {
    return this.director;
  }

  getSummary(): string {
    return this.summary;
  }

  getActors(): string[] {
    return this.actors;
  }
}
