import { Injectable } from '@nestjs/common';

import { DatabaseService } from '../../db/database.service';
import { ParserService } from '../parser/parser.service';

@Injectable()
export class SearchService {
  constructor(
    private readonly databaseService: DatabaseService,
    private readonly parserService: ParserService,
  ) {}

  search(searchQuery: string): string[] {
    let moviesTitles = this.databaseService.fetchUnion(this.parserService.parse(searchQuery));

    return this.databaseService.fetchObjects(moviesTitles);
  }
}
