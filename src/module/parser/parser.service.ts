import { Injectable } from '@nestjs/common';

const fs = require('fs');

@Injectable()
export class ParserService {
  static splitChars = /[\s.,:;?!<>"]+/;

  private readonly stopWords : string[];

  constructor() {
    this.stopWords = this._readStopWordsFromFile();
  }

  parse(query: string): string[] {
    let tokens = query.split(ParserService.splitChars);
    let validTokens = [];

    tokens.forEach((token) => {
      token = token.toLowerCase();
      if (this._isValidToken(token)) {
        validTokens.push(token);
      }
    });

    return validTokens;
  }

  _isValidToken(token: string): boolean {
    return !this.stopWords.includes(token);
  }

  _readStopWordsFromFile(): string[] {
    return fs
      .readFileSync('resources/stop-words.txt')
      .toString()
      .replace(/\r\n/g, '\n')
      .split('\n');
  }
}
