import { DatabaseService } from '../src/db/database.service';
import { ParserService } from '../src/module/parser/parser.service';
import { SearchService } from '../src/module/search/search.service';

function getSearchServiceWithFeededDB() {
    const parserService = new ParserService();
    const databaseService = new DatabaseService();

    databaseService.storeKeyword('a1', 'first word');
    databaseService.storeKeyword('a2', 'second word');

    databaseService.storeKeyword('everyword', 'first word');
    databaseService.storeKeyword('everyword', 'second word');

    databaseService.storeObject('first word', JSON.stringify({ key: 'first word', value: 1 }));
    databaseService.storeObject('second word', JSON.stringify({ key: 'second word', value: 2}));

    return new SearchService(databaseService, parserService);
}

test('search returns correct object', () => {
    const searchService = getSearchServiceWithFeededDB();

    expect(searchService.search('a1')).toStrictEqual([JSON.stringify({ key: 'first word', value: 1 })]);
    expect(searchService.search('a2')).toStrictEqual([JSON.stringify({ key: 'second word', value: 2})]);
});

test('search returns both objects with repeated word', () => {
    const searchService = getSearchServiceWithFeededDB();

    expect(searchService.search('everyword')).toStrictEqual([JSON.stringify({ key: 'first word', value: 1 }), JSON.stringify({ key: 'second word', value: 2})]);
});
