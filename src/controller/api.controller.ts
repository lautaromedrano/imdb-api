import { Controller, Get, Query } from '@nestjs/common';
import { SearchService } from '../module/search/search.service';

@Controller()
export class ApiController {
  constructor(private readonly searchService: SearchService) {}

  @Get()
  search(@Query('search') searchQuery: string): string[] {
    return this.searchService.search(searchQuery);
  }
}
