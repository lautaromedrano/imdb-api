import { Injectable } from '@nestjs/common';

@Injectable()
export class DatabaseService {
  private readonly invertedIndex: Object;
  private readonly objects: Object;

  constructor() {
    this.invertedIndex = {};
    this.objects = {};
  }

  storeObject(key: string, object: string) {
    this.objects[key] = object;
  }

  storeKeyword(key: string, data: string) {
    if (
      this.invertedIndex[key] !== undefined &&
      !this.invertedIndex[key].includes(data)
    ) {
      this.invertedIndex[key].push(data);
    } else {
      this.invertedIndex[key] = [data];
    }
  }

  fetchUnion(keys: string[]): string[] {
    let matches = this.invertedIndex[keys[0]];

    if (!matches) {
      return [];
    }

    for (let i = 1; i < keys.length; i++) {
      matches = matches.filter(
        Set.prototype.has,
        new Set(this.invertedIndex[keys[i]] || []),
      );
    }

    return matches;
  }

  fetchObjects(objectKeys: string[]) {
    let data = [];

    objectKeys.forEach((key) => {
      data.push(this._fetch(key));
    });

    return data;
  }

  _fetch(key: string): string[] {
    return this.objects[key] || [];
  }
}
