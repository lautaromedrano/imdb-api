import { Module } from '@nestjs/common';

import { ApiController } from './controller/api.controller';

import { DatabaseService } from './db/database.service';
import { IndexerService } from './module/indexer/indexer.service';
import { ParserService } from './module/parser/parser.service';
import { SearchService } from './module/search/search.service';

@Module({
  imports: [],
  controllers: [ApiController],
  providers: [DatabaseService, IndexerService, ParserService, SearchService],
})
export class AppModule {}
