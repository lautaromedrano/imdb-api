import { Injectable } from '@nestjs/common';

const axios = require('axios');
const cheerio = require('cheerio');

import { DatabaseService } from '../../db/database.service';
import { MovieDTO } from '../movie/movie.dto';
import { ParserService } from '../parser/parser.service';

const IMDB_BASE_URL = 'https://www.imdb.com';
const IMDB_TOP_1000_URL =
  IMDB_BASE_URL + '/search/title/?groups=top_1000&sort=user_rating&view=simple';

@Injectable()
export class IndexerService {
  constructor(
    private readonly databaseService: DatabaseService,
    private readonly parserService: ParserService,
  ) {
    this._run();
  }

  _run() {
    this._fetchMovies(IMDB_TOP_1000_URL);
  }

  _fetchMovies(url: string) {
    this._fetch(url).then((response) => {
      if (response.status != 200) {
        console.log('Unable to fetch top movies.');
        return;
      }

      let $ = cheerio.load(response.data);
      let nextPageNode = $('.lister-page-next')[0];
      let nextPageLink = nextPageNode ? nextPageNode['attribs']['href'] : null;

      $('.lister-item-header > span:nth-child(2) > a').each((index, node) => {
        let movieUrl = node['attribs']['href'];

        this._fetchMovieData(IMDB_BASE_URL + movieUrl);
      });

      if (nextPageLink) {
        this._fetchMovies(IMDB_BASE_URL + nextPageLink);
      }
    });
  }

  _fetchMovieData(movieUrl: string) {
    this._fetch(movieUrl).then((response) => {
      if (response.status != 200) {
        console.log('Unable to fetch movie data.');
        return;
      }

      let $ = cheerio.load(response.data);
      let title = '';
      let director = '';
      let summary = '';
      let actors = [];

      $('.title_wrapper > h1').each((index, node) => {
        title = node['children'][0]['data'];
      });

      $('.plot_summary div:nth-child(2) > a').each((index, node) => {
        director = node['children'][0]['data'];
      });

      $('.summary_text').each((index, node) => {
        summary = node['children'][0]['data'].trim();
      });

      $('.plot_summary div:nth-child(4) > a[href*=name]').each(
        (index, node) => {
          let actor = node['children'][0]['data'];
          actors.push(actor);
        },
      );

      let movie = new MovieDTO(title, director, summary, actors);
      this.databaseService.storeObject(movie.getTitle(), JSON.stringify(movie));
      this._indexMovieData(movie);
    });
  }

  _indexMovieData(movieData: MovieDTO) {
    let tokens = this.parserService
      .parse(movieData.getTitle())
      .concat(this.parserService.parse(movieData.getDirector()))
      .concat(this.parserService.parse(movieData.getActors().join(' ')))
      .concat(this.parserService.parse(movieData.getSummary()));

    tokens.forEach((token) => {
      this.databaseService.storeKeyword(token, movieData.getTitle());
    });

    console.log(`${movieData.getTitle()} indexed`);
  }

  async _fetch(url: string) {
    return await axios(url).catch((error: string) => console.log(error));
  }
}
